﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GamePlay {
    public interface IAIPlugin {
        string AlLevel
        {
            get;
        }

        Point ptNextMove(CellContent[,] accCurrState, CellContent CurrPlayer);
    }
}
