﻿using GamePlay;
using System.Drawing;

namespace ComputerPlay
{
    public class ComputerPlayPlugin : GamePlay.IAIPlugin
    {
        public string AlLevel
        {
            get
            {
                return "Computer Play";
            }
        }

        public Point ptNextMove(CellContent[,] accCurrState, CellContent CurrPlayer)
        {
            Point ptNewPoint = new Point();
            int iEmpty = -1;
            int iCount = 0;

            for (int i = 0; i < 3; i++)
            {
                iCount = 0;
                iEmpty = -1;
                for (int j = 0; j < 3; j++)
                {
                    if (accCurrState[i, j] == CurrPlayer)
                        iCount++;
                    else if (accCurrState[i, j] == CellContent.Empty)
                        iEmpty = j;
                }
                if (iCount == 2 && iEmpty != -1)
                {
                    ptNewPoint.X = i;
                    ptNewPoint.Y = iEmpty;
                    return ptNewPoint;
                }

            }

            for (int i = 0; i < 3; i++)
            {
                iCount = 0;
                iEmpty = -1;
                for (int j = 0; j < 3; j++)
                {
                    if (accCurrState[j, i] == CurrPlayer)
                        iCount++;
                    else if (accCurrState[j, i] == CellContent.Empty)
                        iEmpty = j;
                }
                if (iCount == 2 && iEmpty != -1)
                {
                    ptNewPoint.X = iEmpty;
                    ptNewPoint.Y = i;
                    return ptNewPoint;
                }
            }
            
            int iCountDia = 0;
            iEmpty = -1;

            for (int i = 0; i < 3; i++)
            {
                int j = i;
                
                if (accCurrState[i, j] == CurrPlayer)
                    iCountDia++;
                else if (accCurrState[i, j] == CellContent.Empty)
                    iEmpty = j;
            }
            if (iCountDia == 2 && iEmpty != -1)
            {
                ptNewPoint.X = iEmpty;
                ptNewPoint.Y = iEmpty;
                return ptNewPoint;
            }
            
            iCountDia = 0;
            iEmpty = -1;
            for (int i = 2; i >= 0; i--)
            {
                int j = 2 - i;
                
                if (accCurrState[i, j] == CurrPlayer)
                    iCountDia++;
                else if (accCurrState[i, j] == CellContent.Empty)
                    iEmpty = j;
                
            }
            if (iCountDia == 2 && iEmpty != -1)
            {
                ptNewPoint.X = 2 - iEmpty;
                ptNewPoint.Y = iEmpty;
                return ptNewPoint;
            }
            
            int iRan = 0;
            int jRan = 0;
            while (iRan < 3)
            {
                
                jRan = 0;
                while (jRan < 3)
                {
                    if (accCurrState[iRan, jRan] == CellContent.Empty)
                    {
                        ptNewPoint.X = iRan;
                        ptNewPoint.Y = jRan;
                        return ptNewPoint;
                    }
                    
                    jRan++;
                }
                iRan++;
            }
          
            return ptNewPoint;
        }
    }
}
